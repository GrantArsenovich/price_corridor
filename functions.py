import numpy as np
import pandas as pd

def calculate_sum_by_currency_rates(df, currency_rates):
    """ Пересчет иностранной валюты """
    mean_by_month = currency_rates.copy()
    # Добавляем столбец с началом месяца
    mean_by_month.loc[:, 'month_start'] = mean_by_month.period.dt.strftime('%Y-%m-01')
    mean_by_month = mean_by_month.groupby(['month_start', 'currency'])['rate'].mean().reset_index()
    mean_by_month = mean_by_month.rename(columns={'rate': 'mean_currency_rate'})
    mean_by_month.month_start = pd.to_datetime(mean_by_month.month_start)
    # Цепляем актуальный курс на каждый день
    df = df.merge(currency_rates, how='left',
                  left_on=['delivery_date', 'currency'],
                  right_on=['period', 'currency'])
    # Цепляем средний курс валют по месяцу
    df = df.merge(mean_by_month, how='left',
                  left_on=['delivery_date_start_month', 'currency'],
                  right_on=['month_start', 'currency'])
    # В случае если курс не определен на определенный день,
    # то в качестве курса в этот день принимаем средний курс за месяц
    df.rate = df.rate.fillna(df.mean_currency_rate)
    # Пересчитываем сумму с иностранной валютой
    df.ttn_sum = np.where((df.currency != 'руб.') & (df.ttn_sum < 5000), df.ttn_sum * df.rate, df.ttn_sum)
    # Удаляем лишние столбцы
    df = df.drop(columns=['period', 'month_start', 'rate', 'mean_currency_rate', 'delivery_date_start_month'])
    return df